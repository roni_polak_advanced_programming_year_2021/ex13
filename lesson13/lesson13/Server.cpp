#include "Server.h"

std::set<std::string> users;
std::condition_variable cv;
std::mutex usersMtx;
std::mutex messagesMtx;

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	std::cout << "binded" << std::endl;

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "listening to port: " << port << std::endl;
	std::thread messagesSend(&Server::messagesSender, this);//calls the messagesSender thread
	messagesSend.detach();
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "accepting client..." << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted !" << std::endl;

	// the function that handle the conversation with the client

	int numBytes;
	std::string message;
	std::string username;
	if (helper.getMessageTypeCode(client_socket) == LOGIN_MESSAGE)//if login message
	{
		numBytes = helper.getIntPartFromSocket(client_socket, DATA_SIZE);
		username = (helper.getStringPartFromSocket(client_socket, numBytes));//get username
		usersMtx.lock();//locks the users mutex
		users.insert(username);//add username to users set
		std::cout << "ADDED new client " << client_socket << " " << username << " to clients list" << std::endl;
		usersMtx.unlock();//unlocks the users mutex
		helper.send_update_message_to_client(client_socket, "", "", getAllUsers());//sends the first update message to the client
	}
	std::thread clientThread(&Server::clientHandler, this, client_socket, username);//creates the clientThread
	clientThread.detach();

}


void Server::clientHandler(SOCKET clientSocket, std::string username)
{
	int numBytes;
	std::string message;
	std::string fullMessage = "";
	std::string secondUsername = "";
	std::string fileName = "";
	messagesStruct myMessage;

	while (true)
	{
		try
		{
			if (helper.getMessageTypeCode(clientSocket) == UPDATE_MESSAGE)//if the client sent an update message
			{
				numBytes = helper.getIntPartFromSocket(clientSocket, DATA_SIZE);
				secondUsername = (helper.getStringPartFromSocket(clientSocket, numBytes));//get the destination username 
				numBytes = helper.getIntPartFromSocket(clientSocket, MESSAGE_DATA_SIZE);
				message = (helper.getStringPartFromSocket(clientSocket, numBytes));//get the message to be sent
				if (message != "")//if there is a message
				{
					fullMessage = "";
					std::unique_lock<std::mutex> locker(messagesMtx);
					fullMessage += "&MAGSH_MESSAGE&&Author&";
					fullMessage += username;
					fullMessage += "&DATA&";
					fullMessage += message;
					//adding to the message struct the information
					myMessage.message = fullMessage;
					myMessage.to = secondUsername;
					myMessage.from = username;
					messages.push(myMessage);//push the message to the queue
					locker.unlock();
					cv.notify_one();//notify the sender thread that there are messages to be sent
				}

				fileName = getFileName(username, secondUsername);
				helper.send_update_message_to_client(clientSocket, getStringFromFile(fileName), secondUsername,  getAllUsers());//send an update message
			}
		}
		catch (const std::exception& e)//a user disconnected
		{
			std::cout << "Exception was caught in function clientHandler. socket= " << clientSocket << " what=" << e.what() << std::endl;
			std::cout << "Recieved exit message from client" << std::endl;
			usersMtx.lock();//lock the users mutex
			std::cout << "REMOVED "<< clientSocket << ", " << username << " from clients list" << std::endl;
			users.erase(username);//remove the user from the set
			usersMtx.unlock();//unlock the users mutex
			closesocket(clientSocket);
			break;//get out of the loop
		}
			
	}

}

void Server::messagesSender()
{
	while (true)
	{
		std::unique_lock<std::mutex> locker(messagesMtx);//lock the unique mutex
		cv.wait(locker);//wait to be notified
		messagesStruct myMessage;
		std::string message;
		if (messages.size() > 0)//make sure again that there are messages to send
		{
			myMessage = messages.front();//get the data from the message
			messages.pop();//pop the message form the queue
		}

		std::string fileName = addMessagetoFile(myMessage.from, myMessage.to, myMessage.message);

		locker.unlock();//unlock the unique mutex
		//message = getStringFromFile(fileName);
	}
}


std::string Server::getAllUsers()
{
	std::string allUsers;
	usersMtx.lock();//lock the users mutex
	for (std::set<std::string>::iterator usersIt = users.begin(); usersIt != users.end(); ++usersIt)//for the set of users
	{
		allUsers += usersIt->data();//add the username
		allUsers += '&';
	}
	if (allUsers.size() > 0)//remove the last char 
	{
		allUsers.pop_back();
	}
	usersMtx.unlock();//unlock the mutex
	return allUsers;
}

std::string Server::addMessagetoFile(std::string username, std::string secondUserName, std::string message)
{
	std::string fileName = "";

	fileName = getFileName(username, secondUserName);//get the filename 


	std::ofstream outputFile;
	outputFile.open(fileName, std::ios_base::app);//open the file with append
	if (outputFile.is_open())//if the file is open
	{

		outputFile << message;//write the message into the file
		outputFile.close();
	}
	else
	{
		std::cout << "Unable to open file";
	}
	return fileName;
}

std::string Server::getStringFromFile(std::string fileName)
{
	std::ifstream MyReadFile(fileName);
	std::string message = "";
	while (std::getline(MyReadFile, message))//turn the file data into a string
	{
	}
	return message;
}

std::string Server::getFileName(std::string username, std::string secondUserName)
{
	std::string fileName = "";
	//get the filename of two users alphabetically
	if (username.compare(secondUserName) < 0)
	{
		fileName += username;
		fileName += '&';
		fileName += secondUserName;
	}
	else
	{
		fileName += secondUserName;
		fileName += '&';
		fileName += username;
	}
	fileName += ".txt";
	return fileName;
}