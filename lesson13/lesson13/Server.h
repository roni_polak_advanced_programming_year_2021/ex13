
#pragma once

#include "Helper.h"
#include <thread>
#include <exception>
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <queue>
#include <mutex>
#include <fstream>

#define LOGIN_MESSAGE 200
#define DATA_SIZE 2
#define UPDATE_MESSAGE 204
#define MESSAGE_DATA_SIZE 5
#define PORT_INDEX 5


struct messagesStruct
{
	std::string message;
	std::string to;
	std::string from;
};

class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:
	/*
	* this method accepts the clients
	*/
	void accept();
	/*
	* this method handles the client and inserst their messages into the queue
	*/
	void clientHandler(SOCKET clientSocket, std::string username);
	/*
	* this method adds the message to the file
	*/
	std::string addMessagetoFile(std::string username, std::string secondUserName, std::string message);
	/*
	* this method sends the messages
	*/
	void messagesSender();
	/*
	* this methof gets all the data from a file in the form of a string
	*/
	std::string getStringFromFile(std::string fileName);
	/*
	* this method gets the fileName of two usera
	*/
	std::string getFileName(std::string username, std::string secondUserName);
	/*
	* this function returns a string of all the users
	*/
	std::string getAllUsers();


	SOCKET _serverSocket;
	std::queue<messagesStruct> messages;
	Helper helper;
};
