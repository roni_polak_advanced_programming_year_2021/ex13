#include "Helper.h"
#include "Server.h"
#include "WSAInitializer.h"

int main()
{
	std::cout << "Starting..." << std::endl;
	try
	{
		std::string portLine = "";
		std::size_t found;
		std::ifstream myFile;
		myFile.open("config.txt");
		while (std::getline(myFile, portLine))
		{
			// Output the text from the file
			found = portLine.find("port=");
			if (found != std::string::npos)
			{
				break;
			}
		}
		myFile.close();
		std::string newPortString = &portLine[5];
		int port = stoi(newPortString);//getting the port number from the config file

		WSAInitializer wsaInit;
		Server myServer;
		//read port form file
		myServer.serve(port);
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}